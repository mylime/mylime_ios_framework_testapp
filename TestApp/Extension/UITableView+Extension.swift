//
//  UITableView+Extension.swift
//  TestApp
//
//  Created by Lavinia Bertuzzi on 14/11/20.
//

import Foundation
import UIKit

public extension UITableView {
    
    func dequeue<T: UITableViewCell>(_: T.Type, for indexPath: IndexPath) -> T {
        // Get class name
        let className = String(describing: T.self)
        // Guard dequeued cell
        guard let cell = self.dequeueReusableCell(withIdentifier: String(describing: T.self), for: indexPath) as? T else {
            debugPrint("The dequeued cell is not an instance of \(className).")
            // Return a default instance for cell
            return T(style: .default, reuseIdentifier: className)
        }
        // Return dequeued cell
        return cell
    }
}
