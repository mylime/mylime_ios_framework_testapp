//
//  ItemTableViewCell.swift
//  TestApp
//
//  Created by Lavinia Bertuzzi on 14/11/20.
//

import UIKit

class ItemTableViewCell: UITableViewCell {
    
    // MARK: - Outlets
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var notesLabel: UILabel!
    
    // MARK: - Public methods
    
    func configure(with name: String?, id: String?, and info: String?) -> ItemTableViewCell {
        // Set title label
        self.nameLabel.text = name
        self.idLabel.text = id
        self.notesLabel.text = info
        // Return self
        return self
    }
}

