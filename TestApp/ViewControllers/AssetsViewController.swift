//
//  AssetsViewController.swift
//  TestApp
//
//  Created by Lavinia Bertuzzi on 14/11/20.
//

import UIKit
import MyLime

class AssetsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    
    var assets: [Asset] = []
    
    // MARK: - UIViewController Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set table view  delegate and data source
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    // MARK: - Actions
    
    @IBAction func goBackAction(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    // MARK: UITableView Delegate and Data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return assets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(ItemTableViewCell.self, for: indexPath)
        return cell.configure(with: "Name: \(self.assets[indexPath.item].name)",
                              id: "ID: \(self.assets[indexPath.item].id)",
                              and: "Notes: \(self.assets[indexPath.item].internalNotes)")
    }
}
