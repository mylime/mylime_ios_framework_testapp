//
//  EventsViewController.swift
//  TestApp
//
//  Created by Lavinia Bertuzzi on 15/11/20.
//

import UIKit

class EventsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    
    var events: [EventUIObject] = []
    
    // MARK: - UIViewController Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set table view delegate and data source
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    // MARK: - Actions
    
    @IBAction func goBackAction(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    // MARK: UITableView Delegate and Data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(ItemTableViewCell.self, for: indexPath)
        var idText: String = "No ID"
        if let id = self.events[indexPath.item].id { idText = "\(id)" }
        return cell.configure(with: "Name: \(self.events[indexPath.item].name ?? "")",
                              id: "ID: \(idText)",
                              and: "Event state: \(self.events[indexPath.item].state ?? "")")
    }
}
