//
//  LoginViewController.swift
//  TestApp
//
//  Created by Osvaldo Pirrello on 14/11/2020.
//

import UIKit
import MyLime

class LoginViewController: UIViewController {
    
    //MARK: - Outlets

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var loginByNfcButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    let url = URL(string: "https://mylime-development.herokuapp.com/api/")
    
    // MARK: - ViewController Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Add gesture recognizer to view
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)

        guard let baseUrl = self.url else {
            showAlert(with: "Error, url is not valid")
            return
        }
        // Configure MyLime
        MyLime.shared.configure(with: baseUrl)
    }
    
    // MARK: - Actions and Selectors

    @IBAction func loginWithCredentials(_ sender: Any) {
        // Dismiss keyboard
        dismissKeyboard()
        // Guard validation for email
        guard let email = emailTextField.text, !email.isEmpty else {
            self.showAlert(with: "Please insert an email address.")
            return
        }
        // Guard validation for password
        guard let password = passwordTextField.text, !password.isEmpty else {
            self.showAlert(with: "Please insert a password.")
            return
        }
        // Start activity indicator
        self.activityIndicator.startAnimating()
        // Disable button
        loginButton.isEnabled = false
        // Perform sign in
        MyLime.shared.signIn(with: email, with: password) { [self] response in
            // Stop activity indicator
            self.activityIndicator.stopAnimating()
            switch response {
            case .success(let model):
                debugPrint(model)
                guard let confirmOtp = model.twoFactorEnabled, confirmOtp else {
                    // Show alert
                    self.showAlert(with: "\(model.firstname ?? "")\(model.lastname ?? "") You are logged in but two factor authentication is not enabled")
                    // Enable button
                    self.loginButton.isEnabled = true
                    return
                }
                // Show confirm otp view
                self.performSegue(withIdentifier: "go_to_confirm_otp_vc", sender: nil)
            case .failure(let error):
                debugPrint(error)
                // Notify error
                self.showAlert(with: "An error occurred -> \(error)")
                // Enable button
                self.loginButton.isEnabled = true
            }
        }
    }
    
    @IBAction func loginByNfc(_ sender: Any) {
        // Perform sign in
        MyLime.shared.signInByNfc { response in
            // Show result
            switch response {
            case .success(let model):
                debugPrint(model)
                guard let confirmOtp = model.twoFactorEnabled, confirmOtp else {
                    // Show alert
                    self.showAlert(with: "\(model.firstname ?? "")\(model.lastname ?? "") You are logged in but two factor authentication is not enabled")
                    // Enable button
                    self.loginButton.isEnabled = true
                    return
                }
                // Show confirm otp view
                self.performSegue(withIdentifier: "go_to_confirm_otp_vc", sender: nil)
            case .failure(let error):
                debugPrint(error)
                // Notify error
                self.showAlert(with: "An error occurred -> \(error)")
                // Enable button
                self.loginByNfcButton.isEnabled = true
            }
        }
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
    // MARK: - Public methods
    
    func showAlert(with text: String) {
        let alert = UIAlertController(title: "Uh-oh", message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        present(alert, animated: true)
    }
}

