//
//  ConfirmOtpViewController.swift
//  TestApp
//
//  Created by Lavinia Bertuzzi on 14/11/20.
//

import UIKit
import MyLime

// MARK: - Struct

struct EventUIObject {
    let id: Int?
    let name: String?
    let state: String?
}

class ConfirmOtpViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var eventIdTextField: UITextField!
    @IBOutlet weak var orderByTextField: UITextField!
    @IBOutlet weak var orderDirection: UITextField!
    
    @IBOutlet weak var confirmOtpButton: UIButton!
    @IBOutlet weak var assetsButton: UIButton!
    @IBOutlet weak var eventsButton: UIButton!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    // MARK: - Properties
    
    var assets: [Asset] = []
    var events: [EventUIObject] = []
    
    // MARK: UIViewController Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Add gesture recognizer to view
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        // Set keyboard type for code text field
        codeTextField.keyboardType = .asciiCapableNumberPad
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? AssetsViewController {
            // Pass assets to next vc
            destinationVC.assets = self.assets
        } else if let destinationVC = segue.destination as? EventsViewController {
            // Pass events to next vc
            destinationVC.events = self.events
        }
    }
    
    // MARK: - Actions and selectors
    
    @IBAction func confirmOtpAction(_ sender: Any) {
        // Guard validation for code
        guard let code = self.codeTextField.text, !code.isEmpty else {
            self.showAlert(with: "Please insert confirm otp code.")
            return
        }
        // Dismiss keyboard
        self.dismissKeyboard()
        // Disable button
        confirmOtpButton.isEnabled = false
        // Start Activity indicator
        self.activityIndicator.startAnimating()
        // Perform confirm otp
        MyLime.shared.confirmOtp(with: code) { response in
            // Stop Activity indicator
            self.activityIndicator.stopAnimating()
            switch response {
            case .success(let user):
                debugPrint(user)
                // Show assets and events button
                self.assetsButton.isHidden = false
                self.eventsButton.isHidden = false
                self.eventIdTextField.isHidden = false
                self.orderByTextField.isHidden = false
                self.orderDirection.isHidden = false
            case .failure(let error):
                debugPrint(error)
                // Notify error
                self.showAlert(with: "An error occurred -> \(error)")
                // Enable button
                self.confirmOtpButton.isEnabled = true
            }
        }
    }
    
    @IBAction func getAssetsAction(_ sender: Any) {
        // Start Activity indicator
        self.activityIndicator.startAnimating()
        // Disable button
        assetsButton.isEnabled = false
        MyLime.shared.assets { response in
            // Stop Activity indicator
            self.activityIndicator.stopAnimating()
            switch response {
            case .success(let model):
                debugPrint(model)
                // Store assets
                self.assets = model
                // Enable button
                self.assetsButton.isEnabled = true
                // Go to asset vc
                self.performSegue(withIdentifier: "go_to_assets_vc", sender: nil)
            case .failure(let error):
                debugPrint(error)
                // Notify error
                self.showAlert(with: "An error occurred -> \(error)")
            }
        }
    }
    
    @IBAction func getEventsAction(_ sender: Any) {
        // Dismiss keyboard
        dismissKeyboard()
        // Start Activity indicator
        self.activityIndicator.startAnimating()
        // Disable button
        eventsButton.isEnabled = false
        // Get assets
        MyLime.shared.event(with: Int(self.eventIdTextField.text ?? ""),
                            orderedBy: self.orderByTextField.text,
                            with: self.orderDirection.text) { reponse in
            // Stop Activity indicator
            self.activityIndicator.stopAnimating()
            switch reponse {
            case .success(let eventsJson):
                debugPrint(eventsJson)
                if let events = eventsJson.value(forKey: "events") as? NSArray {
                    events.forEach { event in
                        if let dictionaryEvent = event as? NSDictionary {
                            self.events.append(self.mapEvent(with: dictionaryEvent))
                        }
                    }
                    // Enable button
                    self.eventsButton.isEnabled = true
                    self.performSegue(withIdentifier: "go_to_events_vc", sender: nil)
                }
            case .failure(let error):
                debugPrint(error)
                // Notify error
                self.showAlert(with: "An error occurred -> \(error)")
            }
        }
    }
    
    @IBAction func goBackAction(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    // MARK: - Public methods
    
    func showAlert(with text: String) {
        let alert = UIAlertController(title: "Uh-oh", message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        present(alert, animated: true)
    }
    
    // MARK: - Private methods
    
    private func mapEvent(with dictionaryEvent: NSDictionary) -> EventUIObject {
        var name: String = ""
        if let data = dictionaryEvent["event_type"] as? NSDictionary {
            name = data["name"] as? String ?? ""
        }
        return EventUIObject(id: dictionaryEvent["id"] as? Int,
                             name: name,
                             state: dictionaryEvent["state"] as? String)
    }
}
