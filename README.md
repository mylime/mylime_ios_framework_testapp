MyLime <https://my-lime.com/>
===================================

What is MyLime iOS Framework testapp?
-------------------

  MyLime iOS Framework testapp is iOS Framework designed for using MyLime platform efficiently and easly.


Quick Links
-----------
- Builds: https://drive.google.com/drive/u/4/folders/15cHuc5ziG2N69UOknhkN-4kfMatoxf-x
- Developer/API Documentation: https://docs.google.com/document/u/1/d/e/2PACX-1vRo1si2PSoeConill1EVNg3tdzbLmwPVJbTTfv_7NZctJ9qdgr3SpT1d_XGL8uIjYPH8OlGIrpoYtXc/pub
